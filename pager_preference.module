<?php

/*
 * @file
 * Pager preference allows users to set the number of results to display in a pager
 * 
 */

/*
 * Implements hook_menu()
 */
function pager_preference_menu($may_cache) {
  $items = array();

  if (!$may_cache) {
    if (arg(0) == 'user-pager-preference' && is_numeric(arg(1)) && is_numeric(arg(2))) {
      $items[] = array(
        'path' => 'user-pager-preference/'. arg(1) .'/'. arg(2),
        'title' => t('Pager Preference'),
        'callback' => 'pager_preference_set_preference',
        'callback arguments' => array(arg(1), arg(2)),
        'access' => pager_preference_set_preference_access(arg(2)),
        'type' => MENU_CALLBACK,
      );
    }
  }
  
  return $items;
}

/*
 * Implements hook_user()
 */
function pager_preference_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'load':
      // Set pager preference for user
      if ($account->uid) {
        $result = db_query('SELECT pager_preference FROM {pager_preference} WHERE uid = %d', $account->uid);
        if (db_num_rows($result)) {
          $pager = db_fetch_object($result);
          $account->pager = $pager->pager_preference;
        }
      }
      break;
  }
}

/*
 * Set pager preference for user
 * 
 * @param $limit
 *  Pager preference to set
 * @param $uid
 *  User id to set preference for
 */
function pager_preference_set_preference($limit, $uid) {
  db_query("UPDATE {pager_preference} SET pager_preference = '%s' WHERE uid = %d", $limit, $uid);
  if (!db_affected_rows()) {
    db_query("INSERT INTO {pager_preference} (uid, pager_preference) VALUES (%d, '%s')", $uid, $limit);
  }
  // Reload the user object
  user_load(array('uid' => $uid));
  
  drupal_goto();
}

/*
 * Menu access callback for pager preference
 * 
 * @param $uid
 *  User id for pager preference being set
 * 
 * @return boolean
 *  TRUE: access is granted
 *  FALSE: access is denied
 *  
 */
function pager_preference_set_preference_access($uid) {
  global $user;
  $access = FALSE;
  if ($uid == $user->uid) {
    $access = TRUE;  
  }
  
  return $access;
}

/*
 * Theme pager preference
 * 
 * @param $uid
 *    User id for the preference being set
 */
function theme_pager_preference_preference($uid) {
  $destination = drupal_get_destination();
  $output = '<div class="pager-preference">';
  $output .= t('Show ') . l(t('25'), 'user-pager-preference/25/'. $uid, array(), $destination) .' / '. l(t('50'), 'user-pager-preference/50/'. $uid, array(), $destination) .' / '. l(t('100'), 'user-pager-preference/100/'. $uid, array(), $destination);
  $output .= '</div>';
  
  return $output;
}

/**
 * Override theme_pager()
 * 
 * Format a query pager.
 *
 * Menu callbacks that display paged query results should call theme('pager') to
 * retrieve a pager control so that users can view other results.
 *
 * @param $tags
 *   An array of labels for the controls in the pager.
 * @param $limit
 *   The number of query results to display per page.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $parameters
 *   An associative array of query string parameters to append to the pager links.
 * @return
 *   An HTML string that generates the query pager.
 *
 * @ingroup themeable
 */
function phptemplate_pager($tags = array(), $limit = 10, $element = 0, $parameters = array()) {
  global $pager_total;
  
  // Check if user has set a pagination preference
  global $user;
  if (!empty($user->pager)) {
    $limit = $user->pager;
  }
  
  $output = '';

  if ($pager_total[$element] > 1) {
    $output .= '<div class="pager">';
    $output .= theme('pager_first', ($tags[0] ? $tags[0] : t('« first')), $limit, $element, $parameters);
    $output .= theme('pager_previous', ($tags[1] ? $tags[1] : t('‹ previous')), $limit, $element, 1, $parameters);
    $output .= theme('pager_list', $limit, $element, ($tags[2] ? $tags[2] : 9 ), '', $parameters);
    $output .= theme('pager_next', ($tags[3] ? $tags[3] : t('next ›')), $limit, $element, 1, $parameters);
    $output .= theme('pager_last', ($tags[4] ? $tags[4] : t('last »')), $limit, $element, $parameters);
    $output .= '</div>';
    
    // Pagination preference
    if ($user->uid) {
      $output .= theme('pager_preference_preference', $user->uid);
    }

    return $output;
  }
}