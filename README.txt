Pager preference allows users to set the number of results to display in a
paginated page throughout the site.

A new div element will be displayed below the pager to allow users to set
their pagination preference simply as:

Show 25 / 50 / 100

pager.inc patch
---------------
Requires patch to pager.inc file located in /YOUR_SITE/includes/ folder to
allow for user's pager preference to override result set limit. Run 
pager.inc.patch against pager.inc.